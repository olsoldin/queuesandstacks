package QueuesAndStacks;

import java.util.*;


/**
 *
 * @author Oliver
 */
public class QueuedStack<E>{

    private E[] stack;


    /**
     * Creates a new Stack with nothing in it
     */
    public QueuedStack(){
        this(0);
    }


    /**
     * Creates an empty stack
     *
     * @param size the size of the empty stack
     */
    public QueuedStack(int size){
        stack = (E[])new Object[size];
    }


    /**
     * Creates a copy of the passed stack
     *
     * @param Stack the Stack to copy
     */
    public QueuedStack(QueuedStack stack){
        this.stack = (E[])(stack == null ? new Object[0] : stack.getArr());
    }


    /**
     * Creates a new Stack with the E array as the values
     *
     * @param Stack the values of the stack
     */
    public QueuedStack(E[] stack){
        this.stack = stack;
    }


    /**
     * Returns whether the Stack contains the given object or not
     *
     * @param o the E to check
     *
     * @return true if the stack contains o
     */
    public boolean contains(Object o){
        for(int i = 0; i < stack.length; i++){
            if(stack[i].equals(o)){
                return true;
            }
        }
        return false;
    }


    /**
     * Returns the number of elements in the Stack
     *
     * @return number of elements in the Stack
     */
    public int size(){
        return stack.length;
    }


    /**
     * Returns <tt>true</tt> if this list contains no elements.
     *
     * @return <tt>true</tt> if this list contains no elements
     */
    public boolean isEmpty(){
        return size() == 0;
    }


    /**
     * Adds the object to the top of the Stack
     *
     * @param o added to the top of the stack
     */
    public void addTop(E o){
        Object[] temp = new Object[stack.length + 1];
        System.arraycopy(stack, 0, temp, 0, stack.length);
        stack = (E[])new Object[temp.length];
        stack = (E[])temp;
        stack[stack.length - 1] = o;
    }


    /**
     * Adds the array to the top of the stack, added in order in the array
     *
     * @param o the array to add to the Stack
     */
    public void addTop(E[] o){
        for(E e : o){
            addTop(e);
        }
    }


    /**
     * Adds the E to the bottom of the Stack
     *
     * @param o the value to be added
     */
    public void addBottom(E o){
        Object[] temp = new Object[stack.length + 1];
        System.arraycopy(stack, 0, temp, 1, stack.length);
        stack = (E[])new Object[temp.length];
        stack = (E[])temp;
        stack[0] = o;
    }


    /**
     * Adds the array to the bottom of the stack, in the order its given in
     *
     * @param o the array to add to the beginning of the stack
     */
    public void addBottom(E[] o){
        //no quick way to do reverse for:each loop
        for(int i = o.length - 1; i >= 0; i--){
            addBottom(o[i]);
        }
    }


    /**
     * Gets the element of the Stack at inex
     * Removes the element from the Stack
     *
     * @param index the index of the element to remove
     *
     * @return the element at index
     */
    public E pop(int index){
        checkBounds(index);
        Object element = stack[index];
        Object[] temp = new Object[stack.length - 1];
        System.arraycopy(stack, 0, temp, 0, index);
        for(int i = index + 1; i < stack.length; i++){
            temp[i - 1] = stack[i];
        }
        stack = (E[])temp;
        return (E)element;
    }


    /**
     * Gets the top element of the Stack
     * Removes the element from the Stack
     *
     * @return the top element
     */
    public E popTop(){
        if(stack.length < 1){
            throw new EmptyStackException();
        }
        Object[] temp = new Object[stack.length - 1];
        System.arraycopy(stack, 0, temp, 0, temp.length);
        E obj = stack[stack.length - 1];
        stack = (E[])temp;
        return obj;
    }


    /**
     * Returns the top "num" amount of items in the stack, will return null if the number is out of boundss
     * Removes the elements from the stack.
     *
     * @param num the number of elements to pop from the top
     *
     * @return the popped elements
     */
    public E[] popTop(int num){
        checkBounds(num);
        Object[] temp = new Object[num];
        for(int i = num - 1; i >= 0; i--){
            temp[i] = popTop();
        }
        return (E[])temp;
    }


    /**
     * Returns the bottom element of the Stack
     * Removes the element from the Stack
     *
     * @return the bottom element of the stack
     */
    public E popBottom(){
        if(stack.length < 1){
            throw new EmptyStackException();
        }
        Object[] temp = new Object[stack.length - 1];
        System.arraycopy(stack, 1, temp, 0, temp.length);
        E obj = stack[0];
        stack = (E[])temp;
        return obj;
    }


    /**
     * Returns the bottom "num" amount of elements
     * Removes them from the Stack
     *
     * @param num the amount of elements to return
     *
     * @return the objects in an array from
     */
    public E[] popBottom(int num){
        checkBounds(num);
        Object[] temp = new Object[num];
        for(int i = 0; i < num; i++){
            temp[i] = popBottom();
        }
        return (E[])temp;
    }


    /**
     * Returns all the elements of the Stack as a single array
     *
     * @return an array of the Stack elements
     */
    public E[] getArr(){
        return stack;
    }


    /**
     * Returns the top element of the Stack, without removing it
     *
     * @return top element of the Stack
     */
    public E getTop(){
        return stack[stack.length - 1];
    }


    /**
     * Returns the top "num" amount of items in the stack, will return null if the number is out of boundss
     * Doesn't remove the elements from the stack.
     *
     * @param num the number of elements to pop from the top
     *
     * @return the popped elements
     */
    public E[] getTop(int num){
        checkBounds(num);
        Object[] temp = new Object[num];
        for(int i = num - 1; i >= 0; i--){
            temp[i] = getTop();
        }
        return (E[])temp;
    }


    /**
     * Returns the bottom element of the Stack, without removing it
     *
     * @return bottom element of the Stack
     */
    public E getBottom(){
        return stack[0];
    }


    /**
     * Returns the bottom "num" amount of elements
     * Doesn't remove them from the Stack
     *
     * @param num the amount of elements to return
     *
     * @return the objects in an array form
     */
    public E[] getBottom(int num){
        checkBounds(num);
        Object[] temp = new Object[num];
        for(int i = 0; i < num; i++){
            temp[i] = getBottom();
        }
        return (E[])temp;
    }


    /**
     * Returns the element at the index position of the stack
     *
     * @param index the element to return
     *
     * @return the element at the given index
     */
    public E get(int index){
        checkBounds(index);
        return stack[index];
    }


    /**
     * Gets the max value in the stack (currently only works with Integers)
     *
     * @return the max value
     */
    public E max(){
        Integer[] intArray;
        try{
            intArray = Arrays.copyOf(stack, stack.length, Integer[].class);
        }catch(ArrayStoreException e){
            throw new UnsupportedOperationException();
        }
        Integer max = 0;
        for(int i = 0; i < intArray.length; i++){
            if(intArray[i] > max){
                max = intArray[i];
            }
        }
        return (E)max;
    }


    /**
     * Gets the min value in the stack (currently only works with Integers)
     *
     * @return the min value
     */
    public E min(){
        Integer[] intArray;
        try{
            intArray = Arrays.copyOf(stack, stack.length, Integer[].class);
        }catch(ArrayStoreException e){
            throw new UnsupportedOperationException();
        }
        Integer min = Integer.MAX_VALUE;
        for(int i = 0; i < intArray.length; i++){
            if(intArray[i] < min){
                min = intArray[i];
            }
        }
        return (E)min;
    }


    /**
     * Sorts the stack using selection sort (only currently works for integers)
     *
     * @return the time it takes to sort the data in nano seconds
     */
    public double sortSelection(){
        Integer[] intArray;
        try{
            intArray = Arrays.copyOf(stack, stack.length, Integer[].class);
        }catch(ArrayStoreException e){
            throw new UnsupportedOperationException();
        }
        double startTime = System.nanoTime();
        //=====================Selection sorting=======================
        /* a[0] to a[n-1] is the array to sort */
        int i, j;
        int iMin;

        /* advance the position through the entire array */
        /*   (could do j < n-1 because single element is also min element) */
        for(j = 0; j < intArray.length - 1; j++){
            /* find the min element in the unsorted a[j .. n-1] */

            /* assume the min is the first element */
            iMin = j;
            /* test against elements after j to find the smallest */
            for(i = j + 1; i < intArray.length; i++){
                /* if this element is less, then it is the new minimum */
                if(intArray[i] < intArray[iMin]){
                    /* found new minimum; remember its index */
                    iMin = i;
                }
            }

            /* iMin is the index of the minimum element. Swap it with the current position */
            if(iMin != j){
                Integer temp = intArray[j];
                intArray[j] = intArray[iMin];
                intArray[iMin] = temp;
            }
        }
        //======================================================================
        double endTime = System.nanoTime();
        stack = (E[])intArray;
        return endTime - startTime;
    }


    /**
     * Sorts the stack using insertion sort (only currently works for integers)
     *
     * @return the time it takes to sort the data in nano seconds
     */
    public double sortInsertion(){
        Integer[] intArray;
        try{
            intArray = Arrays.copyOf(stack, stack.length, Integer[].class);
        }catch(ArrayStoreException e){
            throw new UnsupportedOperationException();
        }
        double startTime = System.nanoTime();
        //===================Insertion sort========================
        /*
         * for i ← 1 to length(A)
         *     j ← i
         *     while j > 0 and A[j-1] > A[j]
         *         swap A[j] and A[j-1]
         *         j ← j - 1
         */
        int i, j;
        for(i = 1; i < intArray.length; i++){
            j = i;
            while(j > 0 && intArray[j - 1] > intArray[j]){
                Integer temp = intArray[j];
                intArray[j] = intArray[j - 1];
                intArray[j - 1] = temp;
                j--;
            }
        }
        //=========================================================
        double endTime = System.nanoTime();
        stack = (E[])intArray;
        return endTime - startTime;
    }


    /**
     * Sorts the stack using bubble sort (only currently works for integers)
     *
     * @return the time it takes to sort the data in nano seconds
     */
    public double sortBubble(){
        Integer[] intArray;
        try{
            intArray = Arrays.copyOf(stack, stack.length, Integer[].class);
        }catch(ArrayStoreException e){
            throw new UnsupportedOperationException();
        }
        double startTime = System.nanoTime();
        //======================Bubble sort=====================
        /* n = length(A)
         * repeat
         *      newn = 0
         *      for i = 1 to n-1 inclusive
         *          if A[i-1] > A[i] then
         *              swap(A[i-1], A[i])
         *              newn = i
         *          end if
         *      end for
         *      n = newn
         * nutil n = 0
         */
        int n, newn;
        n = intArray.length;
        do{
            newn = 0;
            for(int i = 1; i < n; i++){
                if(intArray[i - 1] > intArray[i]){
                    Integer temp = intArray[i];
                    intArray[i] = intArray[i - 1];
                    intArray[i - 1] = temp;
                    newn = i;
                }
            }
            n = newn;
        }while(n > 0);
        //======================================================
        double endTime = System.nanoTime();
        stack = (E[])intArray;
        return endTime - startTime;
    }


    /**
     * Sorts the stack using quick sort (only currently works for integers)
     *
     * @return the time it takes to sort the data in nano seconds
     */
    public double sortQuick(){
        double startTime = System.nanoTime();
        //========================Quick sort=============================
        stack = quickSort(this);
        //===============================================================
        double endTime = System.nanoTime();
        return endTime - startTime;
    }


    /**
     * Uses recursion to sort the elements of the array
     *
     * @param intArray the array to sort
     *
     * @return the sorted array
     */
    private E[] quickSort(QueuedStack intArray){
        /*  function quicksort(array)
         *       if length(array) ≤ 1
         *           return array  // an array of zero or one elements is already sorted
         *       select and remove a pivot element pivot from 'array'  // random position
         *       create empty lists less and greater
         *       for each x in array
         *           if x ≤ pivot then append x to less
         *           else append x to greater
         *       return concatenate(quicksort(less), list(pivot), quicksort(greater)) // two recursive calls
         */
        if(intArray.size() <= 1){
            return (E[])intArray.getArr();
        }
        int pivotPoint = new Random().nextInt(intArray.size());
        QueuedStack less = new QueuedStack();
        Integer pivot = (Integer)intArray.pop(pivotPoint);
        QueuedStack greater = new QueuedStack();

        for(int i = 0; i < intArray.size(); i++){
            Integer currInteger = (Integer)intArray.get(i);
            if(currInteger <= pivot){
                less.addTop(currInteger);
            }else{
                greater.addTop(currInteger);
            }
        }
        return concat(quickSort(less), (E)pivot, quickSort(greater));
    }


    /**
     * Glues 2 arrays together with a single value in between
     *
     * @param arr1 first array
     * @param ell2 middle element
     * @param arr3 last array
     *
     * @return a new array in the form "arr1 + ell2 + arr3"
     */
    private E[] concat(E[] arr1, E ell2, E[] arr3){
        //create a new array of the correct size
        Object[] temp = new Object[arr1.length + 1 + arr3.length];
        //copy the arrays to the created one
        System.arraycopy(arr1, 0, temp, 0, arr1.length);
        temp[arr1.length] = ell2;
        System.arraycopy(arr3, 0, temp, arr1.length + 1, arr3.length);
        //return it as a generic type
        return (E[])temp;
    }


    /**
     * Checks that the given number is greater than 0,
     *  and less than the length of the stack.
     *  throws an IndexOutOfBoundsException
     *
     * @param num the num to check 
     */
    private void checkBounds(int num){
        if(num < 0 || num > stack.length){
            throw new IndexOutOfBoundsException();
        }
    }


    /**
     * Returns all the elements in the Stack as a list in the form:
     * "Element 1\n" +
     * "Element 2\n" +
     * "Element 3\n" +
     * ...
     *
     * If the Stack is empty, it will return "Empty"
     * All leading and trailing whitespace is trimmed
     *
     * @return A String represention of the elements of the Stack
     */
    @Override
    public String toString(){
        String str = "";
        if(stack.length == 0){
            return "Empty";
        }
        for(int i = 0; i < stack.length; i++){
            str += stack[i] + "\n";
        }
        return str.trim();
    }

}
